package Password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class PasswordTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test 
	public void testCheckPasswordLength() {
		int length = Password.checkPasswordLength("123467891011");
		System.out.println("length happy " + length);
		assertTrue("The password length is not valid", length == 12);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testCheckPasswordLengthException() {
		int length = Password.checkPasswordLength("12345");
		System.out.println("length exception " +length);
		assertTrue("The password length is not valid", length == 5);
	}
	
	@Test 
	public void testCheckPasswordLengthBoundaryIn() {
		int length = Password.checkPasswordLength("12345678");
		System.out.println("length boundary in " + length);
		assertTrue("The password length is not valid", length == 8);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testCheckPasswordLengthBoundaryOut() {
		int length = Password.checkPasswordLength("1234567");
		System.out.println("length boundary out " + length);
		assertTrue("The password length is not valid", length == 7);
	}
	
	
	@Test 
	public void testCheckPasswordDigit() {
		int digit = Password.checkPasswordDigit("Abc123xyz");
		System.out.println("digit happy " + digit);
		assertTrue("The password length is not valid", digit == 3);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testCheckPasswordDigitException() {
		int digit = Password.checkPasswordDigit("Abcxyz");
		System.out.println("digit exception " + digit);
		assertTrue("The password length is not valid", digit == 0);
	}
	
	@Test 
	public void testCheckPasswordDigitBoundaryIn() {
		int digit = Password.checkPasswordDigit("Abc12xyz");
		System.out.println("digit boundary in " +digit);
		assertTrue("The password length is not valid", digit == 2);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testCheckPasswordDigitBoundaryOut() {
		int digit = Password.checkPasswordDigit("Abc1xyz");
		System.out.println("digit boundary out " +digit);
		assertTrue("The password length is not valid", digit == 1);
	}
	
	/*@Test
	public void test() {
		fail("Not yet implemented");
	}*/

}
