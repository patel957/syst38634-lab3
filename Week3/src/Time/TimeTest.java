package Time;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class TimeTest {

	public String timeToTest;

	public TimeTest(String dataTime) {
		timeToTest = dataTime;
	}

	@Parameterized.Parameters
	public static Collection <Object [ ]> loadData() {
		Object[][] data = { { "12:05:05" } };
		return Arrays.asList(data);

	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetMilliSeconds() {
		int milli = Time.getMilliSeconds("12:05:05:05");
		System.out.println(milli);
		assertTrue("The Milliseconds were not calculated properly", milli == 5);
	}

	@Test (expected = NumberFormatException.class)
	public void testGetMilliSecondsException() {
		int milli = Time.getMilliSeconds("12:05:05:0054");
		System.out.println(milli);
		assertTrue("The Milliseconds were not calculated properly", milli == 5);
	}

	@Test
	public void testGetMilliSecondsBoundaryIn() {
		int milli = Time.getMilliSeconds("12:05:05:999");
		System.out.println(milli);
		assertTrue("The Milliseconds were not calculated properly", milli == 999);
	}

	@Test (expected = NumberFormatException.class)
	public void testGetMilliSecondsBoundaryOut() {
		int milli = Time.getMilliSeconds("12:05:05:1000");
		System.out.println(milli);
		assertTrue("The Milliseconds were not calculated properly", milli == 0);
	}

	/*@Test
	public void testGetTotalSeconds() {
		// int seconds = Time.getTotalSeconds("12:05:05");
		int seconds = Time.getTotalSeconds(this.timeToTest);
		System.out.println(this.timeToTest);
		assertTrue("The seconds were not calculated propertly", seconds == 43505);
	}
	/*
	 * @Test public void testGetTotalSeconds() { fail("Not yet implemented"); }
	 */

	/*
	 * @Test public void testGetMilliSeconds() { fail("Not yet implemented"); }
	 */

	/*@Test
	public void testGetSeconds() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalMinutes() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalHours() {
		fail("Not yet implemented");
	}*/

}
